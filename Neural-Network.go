package NeuralNetworkGo

import (
	"errors"
	"fmt"
	"math"

    //uncomment it if you don't want to use Matrix API witch atached with this API
    //Matrix "gitlab.com/Ahmed-Hisham/gomatrix"
	Matrix "gitlab.com/Ahmed-Hisham/NeuralNetworkGo/matrix"
)

//NeuralN is a Neural Network
type NeuralN struct {
	inputNodes   int
	outputNodes  int
	weightIH     Matrix.Matrix
	weightHO     Matrix.Matrix
	biasH        Matrix.Matrix
	biasO        Matrix.Matrix
	learningRate float64
}

// Create is a function to create the Artificial Neural Network
func (neural *NeuralN) Create(inputNodes, hiddenNodes, outputNodes int) NeuralN {
	neural.weightIH.Create(hiddenNodes, inputNodes)
	neural.weightIH.Randomize()
	neural.weightHO.Create(outputNodes, hiddenNodes)
	neural.weightHO.Randomize()
	neural.biasH.Create(hiddenNodes, 1)
	neural.biasH.Randomize()
	neural.biasO.Create(outputNodes, 1)
	neural.biasO.Randomize()
	neural.learningRate = 0.5
	neural.inputNodes = inputNodes
	neural.outputNodes = outputNodes
	return *neural
}

// SetLearningRate is a function to set learning rate of the neural network
func (neural *NeuralN) SetLearningRate(value float64) {
	neural.learningRate = value
}

// check is a function that check the input nodes and output nodes to avoid out of range error and some dump mistake
func (neural *NeuralN) check(inputArray, targetArray []float64) {
	if len(inputArray) != neural.inputNodes || len(targetArray) != neural.outputNodes {
		err := errors.New("Number of (Input Nodes / Output Nodes) must equal the length of(Inputted Array / Targeted Array)")
		panic(err)
	}
}

// FeedForword is function te get final result
func (neural *NeuralN) FeedForword(inputArray []float64) Matrix.Matrix {
	if len(inputArray) != neural.inputNodes {
		err := errors.New("Number of \"Input Nodes\" must equal the length of \"Inputted Array\" ")
		panic(err)
	}

	inputs := Matrix.NewFromArray(inputArray)
	hidden, _ := neural.weightIH.StaticDotProduct(inputs)
	hidden.AddFromMatrix(neural.biasH)
	hidden.Map(sigmoid)
	outputs := neural.weightHO
	outputs.DotProduct(hidden)
	outputs.AddFromMatrix(neural.biasO)
	outputs.Map(sigmoid)
	return outputs
}

// Train is function to train the Neural Network
func (neural *NeuralN) Train(inputArray, targetArray []float64) {
	neural.check(inputArray, targetArray)

	targets := Matrix.NewFromArray(targetArray)
	inputs := Matrix.NewFromArray(inputArray)

	hidden, _ := neural.weightIH.StaticDotProduct(inputs)
	hidden.AddFromMatrix(neural.biasH)
	hidden.Map(sigmoid)
	outputs, err := neural.weightHO.StaticDotProduct(hidden)
	outputs.AddFromMatrix(neural.biasO)
	outputs.Map(sigmoid)

	// calculate weights between hidden and outputs
	outputErrors := targets
	outputErrors.SuptractMatrix(outputs)

	// Calculate output gradient
	// X * (1 - X) -> derivativ of sigmoid
	outputsG := outputs
	outputsG.Map(sigmoidD)
	_, err = outputsG.HadProduct(outputErrors)
	if err != nil {
		fmt.Println(outputsG, "\n", outputErrors)
		panic(err)
	}

	outputsG.Multiply(neural.learningRate)

	// Calculate delta
	// Learning rate * Error *
	hiddenT := hidden
	hiddenT.Transpose()
	weightsHOG, err := outputsG.StaticDotProduct(hiddenT)
	if err != nil {
		panic(err)
	}

	// Adjust the weight by delta
	neural.weightHO.AddFromMatrix(weightsHOG)
	// Adjust the bias by gradient
	neural.biasO.AddFromMatrix(outputsG)

	// Calculate hidden layer error
	whoT := neural.weightHO
	whoT.Transpose()
	hiddenErrors, err := whoT.StaticDotProduct(outputErrors)
	if err != nil {
		panic(err)
	}

	// Calculate hidden gradient
	hiddenG := hidden
	hiddenG.Map(sigmoidD)
	_, err = hiddenG.HadProduct(hiddenErrors)
	if err != nil {
		panic(err)
	}
	hiddenG.Multiply(neural.learningRate)

	// Calculate input->hidden deltas
	inputT := inputs
	inputT.Transpose()
	weightHIDelta, err := hiddenG.StaticDotProduct(inputT)
	if err != nil {
		panic(err)
	}

	// Adjust the weight by delta
	neural.weightIH.AddFromMatrix(weightHIDelta)
	// Adjust the bias by gradient
	neural.biasH.AddFromMatrix(hiddenG)

}

//helpfull functions
func sigmoid(n float64) float64 {
	return 1 / (1 + math.Exp(-n))
}

func sigmoidD(n float64) float64 {
	return n * (1 - n)
}
